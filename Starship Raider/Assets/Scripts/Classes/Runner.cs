using UnityEngine;

namespace Classes
{
    public class Runner
    {
        // Cette classe gère le joueur qui sera présent lors de la phase d'infiltration
        private bool grounded;
        private bool boosted;
        
        private int hp;
        private const int maxHp = 1000;

        private Rigidbody2D body;
        private readonly float jumpSpeed;

        public double X => body.position[0];

        public double Y => body.position[1];

        public bool Boosted => boosted;

        public bool Grounded => grounded;

        public int Hp => hp;

        public Runner(Profile profile, Rigidbody2D rigidbody, float jumpSpeed) // maybe add a Map class and put it as a parameter to set x and y
        {
            grounded = false;
            boosted = true;
            body = rigidbody;
            this.jumpSpeed = jumpSpeed;
            this.jumpSpeed = jumpSpeed;
        }

        public void StartInfiltration(float startX, float startY)
        {
            hp = maxHp;
            //body.position = new Vector2(startX, startY);
            grounded = false;
            boosted = false;
        }

        public void ToGround()
        {
            grounded = true;
            boosted = false;
        }

        public void Fall()
        {
            grounded = false;
        }

        public void Jump() // return true if boost is used
        {
            if (grounded) // use classic jump
            {
                body.velocity = new Vector2(body.velocity.x, jumpSpeed);
                grounded = false;
                return;
            }

            if (boosted)
                return;
            body.velocity = new Vector2(body.velocity.x, jumpSpeed); // use boosted jump
            boosted = true;
        }

        public bool GetDamaged(int damage)
        {
            // return true if runner is KO
            hp -= damage;
            Debug.Log(hp);
            if (hp > 0) 
                return false;
            hp = 0;
            return true;
        }
    }
}