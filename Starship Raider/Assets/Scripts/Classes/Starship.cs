﻿using UnityEngine;

namespace Classes
{
    public class Starship // Cette classe sert à gérer les vaisseaux de la phase de confrontation
    {
        public const float AttackForceCost = 50;
        public const float AttackSpeedCost = 35;
        public const float UtilityCost = 60;

        public enum StarshipType
        {
            Fighter,
            Hunter,
            Cargo
        }

        public enum AttackType
        {
            Force,
            Speed,
            Utility
        }

        private readonly StarshipType _type;
        private int hp;
        private int maxHp;
        private readonly int _statAttack;
        private readonly int _statDefense;
        private readonly int _maxAbsorption;
        private int absorption;
        private double energy;
        private Starship _enemy;

        public StarshipType Type => _type;

        public int Hp => hp;

        public int MaxHp => maxHp;

        public int StatAttack => _statAttack;

        public int StatDefense => _statDefense;

        public int MaxAbsorption => _maxAbsorption;

        public int Absorption => absorption;

        public double Energy => energy;

        public Starship Enemy => _enemy;

        public Starship(StarshipType type, int maxHp, int statAttack, int statDefense, int maxAbsorption)
        {
            _type = type;
            this.maxHp = maxHp;
            hp = maxHp;
            _statAttack = statAttack;
            _statDefense = statDefense;
            _maxAbsorption = maxAbsorption;
            absorption = 0;
            energy = 0;
        }

        public void MatchEnemy(Starship starship)
        {
            _enemy = starship;
            hp = maxHp;
            energy = 0;
        }

        public void EndConfrontation()
        {
            _enemy = null;
        }

        public void Refresh(int newHp, int newAbsorption, float newEnergy)
        {
            hp = newHp;
            absorption = newAbsorption;
            energy = newEnergy;
        }

        public void GainEnergy(double value)
        {
            energy += value;
            if (energy > 100) energy = 100;
        }

        private bool Damage(int damage) // return true if the starship is still operational
        {
            hp -= damage;
            if (hp > 0)
                return true;
            hp = 0;
            return false;
        }

        private bool Attack(AttackType attackType, int baseDamage) // return true if the enemy is still operational
        {
            Debug.Log("Action : " + attackType);
            int damage = _statAttack * baseDamage;
            damage /= _enemy._statDefense;
            if ((int) attackType == (int) _type)
                damage = (int) (damage * 1.2);

            if (((int) attackType - (int) _enemy._type) % 3 == 1)
                damage = (int) (damage * 1.2);

            damage -= absorption;
            if (absorption > 0) absorption--;
            return _enemy.Damage(damage);
        }

        public bool AttackForce() // return true if the enemy is still operational
        {
            // we suppose that energy < AttackForceCost
            energy -= AttackForceCost;
            return Attack(AttackType.Force, 70);
        }

        public bool AttackSpeed() // return true if the enemy is still operational
        {
            // we suppose that energy < AttackSpeedCost
            energy -= AttackSpeedCost;
            for (int i = 0; i < 3; i++)
            {
                if (!Attack(AttackType.Speed, 25))
                    return false;
            }

            return true;
        }

        public bool Utility() // return true if the enemy is still operational
        {
            // we suppose that energy < UtilityCost
            energy -= UtilityCost;
            absorption += _type == StarshipType.Cargo ? 5 : 4;
            if (absorption > _maxAbsorption)
                absorption = _maxAbsorption;
            return true;
        }
    }
}