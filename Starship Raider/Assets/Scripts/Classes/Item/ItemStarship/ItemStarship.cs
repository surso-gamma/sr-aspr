namespace Classes.Item.ItemStarship
{
    public class ItemStarship:Item
    {
        // Cette classe gère les items servant à la personnalisation du vaisseau pour la phase d'affrontement
        public ItemStarship(string name, string description, uint price, uint[] upgradeCosts) : base(name, description, price, upgradeCosts)
        {
        }
    }
}