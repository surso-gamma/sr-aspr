﻿namespace Classes.Item
{
    public abstract class Item
    {
        private readonly string name;
        private readonly string description;
        private uint level;
        private readonly uint maxLevel;
        private readonly uint price;
        private readonly uint[] upgradeCosts;

        protected Item(string name, string description, uint price, uint[] upgradeCosts)
        {
            this.name = name;
            this.description = description;
            this.price = price;
            level = 1;
            this.upgradeCosts = upgradeCosts;
            maxLevel = (uint) (1 + this.upgradeCosts.Length);
        }

        public string Name => name;
        public string Description => description;
        public uint Price => price;
        public uint Level => level;
        public uint UpgradeCost => upgradeCosts[level];
        public uint MaxLevel => maxLevel;

        public bool Upgrade()
        {
            if (level >= maxLevel)
                return false;
            level++;
            return true;
        }
    }
}