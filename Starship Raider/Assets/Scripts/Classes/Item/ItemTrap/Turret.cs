 using System;
 using System.Collections.Generic;
 using UnityEngine;

namespace Classes.Item.ItemTrap
{
    public class Turret : ItemTrap
    {
        private GameObject player = GameObject.Find("Player");
        private new const string Description = "This turret shall aim and shoot at any intruders.";
        private readonly double _detectionRange;
        private readonly double reloadTime;
        private double _reloadLeft;
        private readonly double[] _damage;
        private double _orientation;
        private readonly double _rotationSpeed;
        private bool lineOfSight;

        public bool LineOfSight => lineOfSight;
        

        public Turret(GameObject gameObject, uint price, uint[] upgradeCosts, double[] damage, double detectionRange,
            double rotationSpeed, double reloadTime) :
            base(gameObject, "Turret", Description, price, upgradeCosts)
        {
            _detectionRange = detectionRange;
            _damage = damage;
            _rotationSpeed = rotationSpeed;
            this.reloadTime = reloadTime;
        }

        public override void Start(Runner runner)//Utilité
        {
            _reloadLeft = 0;
        }

        private bool Reload()//Cette fonction ne sert à rien pour l'instant
        {
            _reloadLeft -= Time.deltaTime;
            if (_reloadLeft > 0)
                return false;
            _reloadLeft = 0;
            return true;
        }

        private bool Aim(Runner runner)//Utilité
        {
            double dX = runner.X - X;
            double dY = runner.Y - Y;
            double orientationRunner = Math.Atan2(dY, dX) * 180 / Math.PI;
            double delta = (_orientation - orientationRunner + 180) % 360 - 180;
            double rotation = _rotationSpeed * Time.deltaTime;
            if (delta < 0)
            {
                if (rotation >= -delta)
                {
                    _orientation = orientationRunner;
                    return true;
                }

                _orientation += rotation;
                _orientation %= 360;
                return false;
            }

            if (delta > 0)
            {
                if (rotation >= delta)
                {
                    _orientation = orientationRunner;
                    return true;
                }

                _orientation -= rotation;
                _orientation %= 360;
                return false;
            }

            return true;
        }

        public override void Update(Runner runner)
        {
            Transform playerpos = player.transform;
            Vector2 playerdirection = playerpos.position - GameObject.transform.position;
            RaycastHit2D rayinfo = Physics2D.Raycast(GameObject.transform.position, playerdirection, (float)_detectionRange);
            Transform Diode = GameObject.transform.GetChild(1).transform.GetChild(0).transform.GetChild(0);
            if (rayinfo)
            {
                if (rayinfo.collider.gameObject.CompareTag("Player") && rayinfo.distance < _detectionRange)
                {
                    Action(runner);//Utilité
                    Diode.GetComponent<SpriteRenderer>().color = Color.red;
                    lineOfSight = true;
                    Transform Gun = GameObject.transform.GetChild(1).transform.GetChild(1).transform;
                    Gun.up = playerdirection;
                }
            }
            else
            {
                Diode.GetComponent<SpriteRenderer>().color = Color.green;
                lineOfSight = false;
            }
        }

        private void Action(Runner runner)//Utilité
        {
            bool reloaded = Reload();
            double dX = runner.X - X;
            double dY = runner.Y - Y;
            double distance = Math.Pow(dX, 2) + Math.Pow(dY, 2);
            if (distance > Math.Pow(_detectionRange, 2) || !Aim(runner) || !reloaded)
                return;
            _reloadLeft = reloadTime;
        }
    }
}