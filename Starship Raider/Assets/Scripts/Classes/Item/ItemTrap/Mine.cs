﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Classes.Item.ItemTrap
{
    public class Mine : ItemTrap
    {
        private new const string Description = "Intellignet mine that explodes when the player is the closest to it.";
        private readonly double _detectionRange;
        private double _bestDistance;
        private bool _armed;
        private readonly double[] _damage;

        public Mine(GameObject gameObject, uint price, uint[] upgradeCosts, double[] damage, double detectionRange) :
            base(gameObject, "Mine", Description, price, upgradeCosts)
        {
            _detectionRange = detectionRange;
            _damage = damage;
        }

        public override void Start(Runner runner)
        {
            _armed = true;
            _bestDistance = _detectionRange;
        }

        private void Explode(double distance, Runner runner)
        {
            _armed = false;
            double d = _damage[Level-1] * _detectionRange / (_detectionRange + distance);
            runner.GetDamaged((int) d);
        }

        public override void Update(Runner runner)
        {
            if (!_armed)
                return;
            double dX = runner.X - X;
            double dY = runner.Y - Y;
            double distance = Math.Pow(dX, 2) + Math.Pow(dY, 2);
            if (distance > _detectionRange)
                return;
            if (distance <= _bestDistance)
                _bestDistance = distance;
            else
                Explode(_bestDistance, runner);
        }
    }
}