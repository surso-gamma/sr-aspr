﻿using UnityEngine;
using UnityEngine.UI;

namespace Classes.Item.ItemTrap
{
    public abstract class ItemTrap : Item
    {
        private GameObject gameObject;
        private float x;
        private float y;

        public GameObject GameObject => gameObject;

        public float X => x;

        public float Y => y;

        protected ItemTrap(GameObject gameObject, string name, string description, uint price, uint[] upgradeCosts) : base(name,
            description, price, upgradeCosts)
        {
            this.gameObject = gameObject;
        }

        private void Place(float newX, float newY)
        {
            x = newX;
            y = newY;
        }

        public abstract void Start(Runner runner);

        public abstract void Update(Runner runner);
    }
}