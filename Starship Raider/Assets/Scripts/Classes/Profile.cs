using System.Collections.Generic;
using UnityEngine;

namespace Classes
{
    public class Profile
    {
        private static readonly uint[] xpTable =
            {0, 20, 70, 170, 370, 770, 1_770, 3_770, 8_770, 18_770, 38_770, 73_770, 123_770, 203_770};

        private string name;
        private uint credits;
        private uint scraps;
        private uint xp;
        private uint level;
        private Starship mainStarship;
        private List<Item.Item> inventory;
        private List<Starship> garage;

        public Profile(string name)
        {
            this.name = name;
            credits = 100;
            scraps = 20;
            xp = 0;
            level = 1;
            inventory = new List<Item.Item>();
            mainStarship = new Starship(Starship.StarshipType.Fighter, 100, 20, 20, 10);
            garage = new List<Starship> {mainStarship};
        }

        public Profile(string name, uint credits, uint scraps, uint xp, List<Item.Item> inventory, List<Starship> garage,
            Starship mainStarship)
        {
            this.name = name;
            this.credits = credits;
            this.scraps = scraps;
            this.xp = xp;
            level = GetLevel();
            this.inventory = inventory;
            this.garage = garage;
            this.mainStarship = mainStarship;
        }

        public string Name => name;

        public uint Credits => credits;

        public uint Scraps => scraps;

        public uint Xp => xp;

        public uint Level => level;

        public Starship MainStarship => mainStarship;

        public List<Item.Item> Inventory => inventory;

        public List<Starship> Garage => garage;

        public bool ChooseMainStarship(int index)
        {
            if (index < 0 || index >= garage.Count) return false;
            mainStarship = garage[index];
            return true;
        }

        public Runner InitiateRunner(Rigidbody2D rigidbody, float jumpSpeed)
        {
            return new Runner(this, rigidbody,jumpSpeed);
        }

        private bool EarnXp(uint value) // return true if new level reached
        {
            xp += value;
            if (level >= xpTable.Length || xp < xpTable[level]) return false;
            level++;
            return true;
        }

        private uint GetLevel() // get the level corresponding to a certain amount of xp
        {
            uint getLevel = 0;
            while (getLevel < xpTable.Length && xp >= xpTable[getLevel]) getLevel++;
            return getLevel;
        }

        private bool Buy(Item.Item item) // return true if the operation is successful
        {
            if (item.Price > credits)
                return false;
            credits -= item.Price;
            inventory.Add(item);
            return true;

        }

        private bool Upgrade(Item.Item item) // return true if the operation is successful
        {
            if (item.UpgradeCost > scraps || !item.Upgrade()) return false;
            scraps -= item.UpgradeCost;
            return true;
        }
    }
}