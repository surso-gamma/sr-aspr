﻿using Classes;

namespace Menus
{
    public static class LoadProfile
    {
        private static Profile _profile;

        public static Profile Load()
        {
            if (_profile != null)
            {
                return _profile;
            }

            // TODO: read a file
            _profile = new Profile("Player 1");
            return _profile;
        }
    }
}