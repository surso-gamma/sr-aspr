using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Menus
{
    public class DisplaySettings : MonoBehaviour
    {
        private Resolution[] _resolutions;
        public TMP_Dropdown resolutionDropdown;

        public TMP_Text resolutionLabel;

        public Toggle vsync;
        // Start is called before the first frame update
        void Start()
        {
            _resolutions = Screen.resolutions;
        
            resolutionDropdown.ClearOptions();

            List<string> options = new List<string>();

            int currentResolutionIndex = 0;
            for (int i = 0; i < _resolutions.Length; i++)
            {
                string option = _resolutions[i].width + "x" + _resolutions[i].height;
                options.Add(option);

                if (_resolutions[i].width == Screen.currentResolution.width && _resolutions[i].height == Screen.currentResolution.height)
                {
                    currentResolutionIndex = i;
                }
            }
        
            resolutionDropdown.AddOptions(options);
            resolutionDropdown.value = currentResolutionIndex;
            resolutionDropdown.RefreshShownValue();
        
            if (QualitySettings.vSyncCount == 0)
            {
                vsync.isOn = false;
            }
            else
            {
                vsync.isOn = true;
            }

        }

        public void SetResolution(int resolutionIndex)
        {
            Resolution resolution = _resolutions[resolutionIndex];
            Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
        }

        // Update is called once per frame
        void Update()
        {
        
        }

        public void SetFullScreen(bool isFullscreen)
        {
            Screen.fullScreen = isFullscreen;
        }

        public void SetVsync(bool isVsync)
        {
            if (vsync.isOn)
            {
                QualitySettings.vSyncCount = 1;
            }
            else
            {
                QualitySettings.vSyncCount = 0;
            }
        }
    }
}