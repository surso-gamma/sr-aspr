using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Menus
{
    public class KeybindingsManager : MonoBehaviour
    {
        private Dictionary<string, KeyCode> keys = new Dictionary<string, KeyCode>();

        public Text right, left, jump;

        private GameObject currentKey;
    
    
        private Color32 normal = new Color32(0, 0, 0, 255);
        private Color32 selected = new Color32(87, 45, 132, 255);
    
        // Start is called before the first frame update
        void Start()
        {
            keys.Add("Jump", KeyCode.Space);
            keys.Add("Left", KeyCode.Q);
            keys.Add("Right", KeyCode.D);

            jump.text = keys["Jump"].ToString(); 
            left.text = keys["Left"].ToString(); 
            right.text = keys["Right"].ToString(); 
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(keys["Jump"]))
            {
                Debug.Log("Jump");
            }
            if (Input.GetKeyDown(keys["Left"]))
            {
                Debug.Log("Left");
            }
            if (Input.GetKeyDown(keys["Right"]))
            {
                Debug.Log("Right");
            }
        }

        //OnGUI changes the key shown in the button.
        private void OnGUI()
        {
            if (currentKey != null)
            {
                Event e = Event.current;
                if (e.isKey)
                {
                    keys[currentKey.name] = e.keyCode;
                    currentKey.transform.GetChild(0).GetComponent<Text>().text = e.keyCode.ToString();
                    currentKey.GetComponent<Image>().color = normal;
                    currentKey = null;
                }
            }
        }
    
        //changeKey applies the highlight on the button of the key to rebind.
        public void changeKey(GameObject clicked)
        {
            if (currentKey != null)
            {
                currentKey.GetComponent<Image>().color = normal;
            }
            currentKey = clicked;
            currentKey.GetComponent<Image>().color = selected;
        }
    }
}