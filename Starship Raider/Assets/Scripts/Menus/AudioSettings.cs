using UnityEngine;
using UnityEngine.Audio;

namespace Menus
{
    public class AudioSettings : MonoBehaviour
    {
        public AudioMixer audioMixer;
    
        public void SetVolume(float volume)
        {
            audioMixer.SetFloat("Volume", volume);
        }
    }
}