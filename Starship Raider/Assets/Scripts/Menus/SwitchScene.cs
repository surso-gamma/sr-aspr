using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;

namespace Menus
{
    public class SwitchScene : MonoBehaviour
    {
        public void LoadScene(string scene)
        {
            if (PhotonNetwork.IsConnected)
            {
                Debug.Log("Disconnecting...");
                PhotonNetwork.Disconnect();
            }
            SceneManager.LoadScene(scene);
        }
    }
}