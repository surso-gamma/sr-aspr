using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.SceneManagement;

public class RunnerWin : MonoBehaviour
{
    public GameObject runnerWinUI;
    private float timer = 5;
    private bool HasWon = false;
    private void OnTriggerEnter2D(Collider2D other)
    {
        runnerWinUI.SetActive(true);
        HasWon = true;
    }

    void Update()
    {
        if (HasWon)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
                BackToMenu();
        }
    }

    private void BackToMenu()
    {
        SceneManager.LoadScene("Main menu");
    }
}
