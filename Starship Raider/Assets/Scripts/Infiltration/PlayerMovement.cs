using System;
using Classes;
using UnityEngine;
using UnityEngine.UI;

namespace Infiltration
{
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private float jumpSpeed;
        [SerializeField] private float speed;
        public int HP;
        private Rigidbody2D body;
        private Animator anim;
        private bool grounded;
        private Runner runner;
        private Profile profile;
        private bool jumpPressed;
        private bool falling;
        public Text scoreText;

        public Runner Runner => runner;

        // Start is called before the first frame update
        void Start()
        {
            profile = new Profile("Username");
            //Gets references for rigibody2D and animator from player object
            body = GetComponent<Rigidbody2D>();
            anim = GetComponent<Animator>();
            runner = profile.InitiateRunner(body, jumpSpeed);
            runner.StartInfiltration(-10, -8);
            HP = runner.Hp;
            jumpPressed = false;
            falling = false;
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            HP = runner.Hp;
            float horizontalInput = Input.GetAxis("Horizontal"); //TODO: Replace with a variable input  
            body.velocity = new Vector2(horizontalInput * speed, body.velocity.y);

            //Flips the player's sprite whenever it moves left or right
            if (horizontalInput > 0.01f) // if the player moves right
            {
                transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            }
            else if (horizontalInput < -0.01f) // if the player moves left
            {
                transform.localScale = new Vector3(-0.5f, 0.5f, 0.5f);
            }

            if (Input.GetKey(KeyCode.UpArrow)) //Jump call
            {
                if (!jumpPressed)
                {
                    runner.Jump();
                    jumpPressed = true;
                }
            }
            else
                jumpPressed = false;

            //Set animator parameters
            anim.SetBool("Run", horizontalInput != 0);
            anim.SetTrigger("Jump");
            anim.SetBool("grounded", !runner.Boosted);
            if (falling)
                runner.Fall();
            falling = true;
            
            
            scoreText.text = Convert.ToString(HP);
        }

        //Checks if Player is grounded on Ground object
        private void OnCollisionStay2D(Collision2D collision)
        {
            //OnTop = collision.gameObject.CompareTag("TopSide");
            if (collision.gameObject.CompareTag("Ground"))
            {
                runner.ToGround();
                falling = false;
            }
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.collider.tag == "Bullet")
            {
                if (HP <= 0)
                {
                    FindObjectOfType<RunnerLoss>().RunnerHasLost();
                    Destroy(this.gameObject);
                }
            }
        }
    }
}