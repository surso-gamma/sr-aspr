using System;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;

    public float smoothSpeed = 0.0001f;
    public Vector3 offset ;

    private void FixedUpdate()
    {
        if (target)
        {
            Vector3 niceposition = target.position + offset;
            Vector3 smoothing = Vector3.Lerp(transform.position, niceposition, smoothSpeed);
            transform.position = smoothing;
        }
    }
}
