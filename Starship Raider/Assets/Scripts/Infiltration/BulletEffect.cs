using System;
using System.Runtime.CompilerServices;
using Classes.Item.ItemTrap;
using Infiltration;
using UnityEngine;

public class BulletEffect : MonoBehaviour
{
    public GameObject explosion;
    private TurretAction turret;
    private int damage;

    private void Start()
    {
        turret = gameObject.GetComponentInParent<TurretAction>();
        damage = (int)turret.Damage[turret.CurrentLevel];
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(collision.collider.name);
        Instantiate(explosion,transform.position,Quaternion.identity);
        Debug.Log(collision.gameObject.name);
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<PlayerMovement>().Runner.GetDamaged(damage);
        }
        gameObject.GetComponent<TrailRenderer>().emitting = false;
        Destroy(gameObject);
    }
}
