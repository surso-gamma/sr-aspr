using Classes;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.SceneManagement;


public class RunnerLoss : MonoBehaviour
{
    public GameObject runnerLostUI;
    private float timer = 5;
    private bool HasLost = false;
    
    public void RunnerHasLost()
    {
        runnerLostUI.SetActive(true);
        HasLost = true;
    }

    void Update()
    {
        if (HasLost)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
                BackToMenu();
        }
    }

    private void BackToMenu()
    {
        SceneManager.LoadScene("Main menu");
    }
}