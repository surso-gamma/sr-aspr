using System.Collections;
using Classes;
using Classes.Item.ItemTrap;
using Photon.Pun.Demo.Asteroids;
using UnityEngine;
using UnityEngine.UI;

namespace Infiltration
{
    public class TurretAction : MonoBehaviour
    {
        public GameObject playerTarget;
        public GameObject turretGameObject;
        public GameObject Bullet;
        public Transform ShootPoint;
        private GameObject muzzleFlash1;
        private GameObject muzzleFlash2;
        private GameObject muzzleFlash3;
        private GameObject muzzleFlash4;
        private Turret turret;
        private int currentLevel = 1; //Doit être définit par le profile du joueur.
        private double[] damage = new[] {70d, 80d, 100d,130d,180d};
        private uint[] upgradeCost = new uint[] {20, 50, 90, 150};
        private uint Price = 100;
        private double detectionRange = 7;
        private double rotationSpeed = 30;
        private double reloadTime = 1;
        private double NextBullet = 0;
        private Profile profile;
        private float bulletspeed = 400;
        
        //Player Controlled Turret
        private bool playerControlled;
        private bool Firing;
        private Camera camera;
        private Transform gun;

        public double[] Damage => damage;
        public int CurrentLevel => currentLevel;
        void Start()
        {
            playerControlled = false;
            gun = turretGameObject.transform.GetChild(1).transform.GetChild(1);
            Rigidbody2D playerTargetbody = playerTarget.GetComponent<Rigidbody2D>();
            Runner runner = new Runner(profile, playerTargetbody, 8);// Le 8 selon le GameObject de player et le script PlayerMovement 
            turret = new Turret(turretGameObject, Price, upgradeCost, damage, detectionRange, rotationSpeed,reloadTime);
            turret.Start(runner);//Utilité
            muzzleFlash1 = turretGameObject.transform.GetChild(1).transform.GetChild(1).transform.GetChild(0).transform.GetChild(0).gameObject;
            muzzleFlash2 = turretGameObject.transform.GetChild(1).transform.GetChild(1).transform.GetChild(0).transform.GetChild(1).gameObject;
            muzzleFlash3 = turretGameObject.transform.GetChild(1).transform.GetChild(1).transform.GetChild(0).transform.GetChild(2).gameObject;
            muzzleFlash4 = turretGameObject.transform.GetChild(1).transform.GetChild(1).transform.GetChild(0).transform.GetChild(3).gameObject;
            for (int i = 0; i < Camera.allCamerasCount; i++)
            {
                if (Camera.allCameras[i].name == "Trapper Camera")
                {
                    camera = Camera.allCameras[i];
                }
            }
        }
        // Reload, Aim Shoot
        void Update()
        {
            if (playerControlled)
            {
                Firing = Input.GetMouseButton(0);
                Vector3 mousePositionOnScreen = Input.mousePosition;
                Vector3 mousePositionInWorld = camera.ScreenToWorldPoint(mousePositionOnScreen);
                Debug.Log("Gun is at " + gun.position.x + " in x and " + gun.position.y + " in y");
                Debug.Log("Turret is at " + turretGameObject.transform.position.x + " in x and " + turretGameObject.transform.position.y + " in y");
                Vector2 playerdirection = mousePositionInWorld - gun.position;
                gun.up = playerdirection;
                if (NextBullet < Time.time && Firing)
                {
                    NextBullet = Time.time + reloadTime;
                    Shoot(mousePositionInWorld);
                    StartCoroutine(MuzzleFlash());
                }
            }
            else
            {
                if (playerTarget)
                {
                    Rigidbody2D playerTargetbody = playerTarget.GetComponent<Rigidbody2D>();
                    turret.Update(new Runner(profile, playerTargetbody, 8));
                    if (NextBullet < Time.time && turret.LineOfSight)
                    {
                        NextBullet = Time.time + reloadTime;
                        Shoot(playerTarget.transform.position);
                        StartCoroutine(MuzzleFlash());
                    }
                }
            }
        }
        
        void Shoot(Vector3 target)
        {
            GameObject NewBullet = Instantiate(Bullet,ShootPoint.position, Quaternion.identity,turretGameObject.transform);
            Vector2 bulletDirection = target - gun.position;
            bulletDirection.Normalize();
            bulletDirection *= bulletspeed;
            NewBullet.GetComponent<Rigidbody2D>().AddForce(bulletDirection);
        }

        IEnumerator MuzzleFlash()
        {
            Debug.Log(muzzleFlash1.name + "///" + muzzleFlash2.name + "///" + muzzleFlash3.name + "///");
            muzzleFlash1.SetActive(true);
            yield return new WaitForSeconds(0.05f);
            muzzleFlash1.SetActive(false);
            muzzleFlash2.SetActive(true);
            yield return new WaitForSeconds(0.05f);
            muzzleFlash2.SetActive(false);
            muzzleFlash3.SetActive(true);
            yield return new WaitForSeconds(0.05f);
            muzzleFlash3.SetActive(false);
            muzzleFlash4.SetActive(true);
            yield return new WaitForSeconds(0.05f);
            muzzleFlash4.SetActive(false);
        }
    }
}

