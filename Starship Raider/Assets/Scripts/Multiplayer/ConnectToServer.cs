using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;
using Photon.Pun;
using Photon.Realtime;

namespace Multiplayer
{
    public class ConnectToServer : MonoBehaviourPunCallbacks
    {
    
        //Connection to the Photon server
        private void Start()
        {
            PhotonNetwork.AutomaticallySyncScene = true;
            PhotonNetwork.ConnectUsingSettings();
        }
        //Join the lobby when client is connected to server
        public override void OnConnectedToMaster()
        {
            PhotonNetwork.JoinLobby();
        }
        //Load scene "Waiting Room" when client has joined the lobby
        public override void OnJoinedLobby()
        {
            SceneManager.LoadScene("MultiplayerLobby");
            PhotonNetwork.NickName = "Player " + Random.Range(0, 10000).ToString("0000");
        }
    }
}