using Photon.Pun;
using UnityEngine;
using TMPro;

namespace Multiplayer
{
    public class InWaitingRoom : MonoBehaviourPunCallbacks
    {
        [SerializeField] public float timeLeft;
        [SerializeField] public GameObject Countdown;
        private TMP_Text text;

        private void Start()
        {
            timeLeft = 5;
            Countdown.SetActive(false);
            text = Countdown.GetComponent<TMP_Text>();
        }

        void Update()
        {
            CountdownUpdate();
            if (PhotonNetwork.PlayerList.Length < 2)
            {
                Countdown.SetActive(false);
                timeLeft = 5;
                return;
            }

            Countdown.SetActive(true);
            timeLeft -= Time.deltaTime;
            if (timeLeft <= 0 && PhotonNetwork.IsMasterClient)
                PhotonNetwork.LoadLevel("Confrontation");
        }

        private void CountdownUpdate()
        {
            text.text = "Beginning in: " + (1 + (int) timeLeft);
        }
    }
}