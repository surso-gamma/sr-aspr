using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

namespace Multiplayer
{
    public class Matchmaking : MonoBehaviourPunCallbacks
    {
        [SerializeField] private Transform playerListContent;
        [SerializeField] private GameObject PlayerListItemPrefab;

        public void Connect()
        {
            if (PhotonNetwork.IsConnected)
            {
                PhotonNetwork.JoinRandomRoom();
            }
            else
            {
                PhotonNetwork.ConnectUsingSettings();
            }
        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            PhotonNetwork.CreateRoom(null, new RoomOptions() {MaxPlayers = 2});
            Debug.Log("Room not found, creating one.");
        }

        public override void OnJoinedRoom()
        {
            Debug.Log("Client is now in a room.");
            PhotonNetwork.LoadLevel("Waiting Room");
        }
    }
}