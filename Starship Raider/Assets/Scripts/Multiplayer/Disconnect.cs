using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Multiplayer
{
    public class Disconnect : MonoBehaviour
    {

        public void DisconnectFromServer()
        {
            Debug.Log("Disconnecting...");
            PhotonNetwork.Disconnect();
            SceneManager.LoadScene("Main menu");
        }

    }
}