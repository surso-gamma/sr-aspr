﻿using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Multi
{
    public class GameManager : MonoBehaviourPunCallbacks
    {
        public override void OnLeftRoom()
        {
            SceneManager.LoadScene("Launcher");
        }

        public void LeaveRoom()
        {
            PhotonNetwork.LeaveRoom();
        }

        void LoadScene()
        {
            if (!PhotonNetwork.IsMasterClient)
            {
                Debug.LogError("LoadScene: Trying to Load a level but we are not the master Client");
            }

            if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                Debug.Log("The other player has disconnected. Winning by forfeit");
                // TODO: Player Has Won. Give him his money.
                LeaveRoom();
                Debug.Log("Loading scene: Main Menu");
                SceneManager.LoadScene("Main Menu");
                return;
            }

            if (SceneManager.GetActiveScene().name == "Waiting Room")
            {
                Debug.Log("Loading scene: Confrontation");
                PhotonNetwork.LoadLevel("Confrontation");
                return;
            }

            Debug.Log("Loading scene: Waiting Room");
            PhotonNetwork.LoadLevel("Waiting Room");
        }

        public override void OnPlayerEnteredRoom(Player other)
        {
            Debug.Log($"{other.NickName} entered the room");
            if (PhotonNetwork.IsMasterClient)
            {
                Debug.Log("MasterClient load a new scene");
                LoadScene();
            }
        }

        public override void OnPlayerLeftRoom(Player other)
        {
            Debug.Log($"{other.NickName} left the room");
            if (PhotonNetwork.IsMasterClient)
            {
                Debug.Log("MasterClient load a new scene");
                LoadScene();
            }
        }
    }
}