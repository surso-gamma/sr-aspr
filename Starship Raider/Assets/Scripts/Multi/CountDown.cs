﻿using Photon.Pun;
using TMPro;
using UnityEngine;

namespace Multi
{
    public class CountDown:MonoBehaviourPunCallbacks
    {
        [SerializeField] public float timeLeft;
        private TMP_Text text;

        private void Start()
        {
            timeLeft = 5;
            gameObject.SetActive(false);
            text = gameObject.GetComponent<TMP_Text>();
        }

        void Update()
        {
            CountdownUpdate();
            if (PhotonNetwork.CurrentRoom.PlayerCount < 2)
            {
                gameObject.SetActive(false);
                timeLeft = 5;
                return;
            }

            gameObject.SetActive(true);
            timeLeft -= Time.deltaTime;
            if (timeLeft <= 0 && PhotonNetwork.IsMasterClient)
                PhotonNetwork.LoadLevel("Confrontation");
        }

        private void CountdownUpdate()
        {
            text.text = "Beginning in: " + (1 + (int) timeLeft);
        }
    }
}