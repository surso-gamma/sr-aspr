﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

namespace Confrontation.Updates
{
    public class UpdateEnergy : MonoBehaviour
    {
        [PunRPC]
        void SetEnergy(double value)
        {
            GetComponent<Image>().fillAmount = (float) value / 100;
        }
    }
}