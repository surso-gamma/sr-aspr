﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

namespace Confrontation.Updates
{
    public class UpdateHealth:MonoBehaviour
    {
        [PunRPC]
        void SetHp(int value)
        {
            GetComponent<Slider>().value = value;
        }
    }
}