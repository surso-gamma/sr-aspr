﻿using System;
using UnityEngine;
using Random = System.Random;

namespace Confrontation
{
    public class Test : MonoBehaviour
    {
        public enum Direction
        {
            Right,
            Up,
            Left,
            Down
        }

        public Direction _direction;
        public double _timeLeft;
        private readonly Random _random = new Random();
        public float speed;
        public double xMin;
        public double xMax;
        public double yMin;
        public double yMax;

        private void Start()
        {
            transform.position = Vector2.zero;
            _direction = Direction.Right;
            _timeLeft = 0;
        }

        private void ChangeDirection()
        {
            int n = (int) _direction;
            _direction = (Direction) ((n + 2) % 4);
        }

        private void CheckBounds()
        {
            Vector3 position = transform.position;
            double x = position[0];
            double y = position[1];
            if (x > xMax || x < xMin || y > yMax || y < yMin)
                ChangeDirection();
        }

        private void Move(Direction direction)
        {
            float distance = speed * Time.deltaTime;
            switch (direction)
            {
                case Direction.Right:
                    transform.position += distance * Vector3.right;
                    break;
                case Direction.Up:
                    transform.position += distance * Vector3.up;
                    break;
                case Direction.Left:
                    transform.position += distance * Vector3.left;
                    break;
                case Direction.Down:
                    transform.position += distance * Vector3.down;
                    break;
                default:
                    return;
            }
        }

        private void Update()
        {
            _timeLeft -= Time.deltaTime;
            if (_timeLeft <= 0)
            {
                _direction = (Direction) _random.Next(4);
                _timeLeft = _random.NextDouble() * 2 + 2;
            }
            CheckBounds();
            Move(_direction);
        }

        private void OnCollisionEnter(Collision collision)
        {
            ChangeDirection();
        }
    }
}