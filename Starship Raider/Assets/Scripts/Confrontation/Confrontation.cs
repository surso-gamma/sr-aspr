﻿using System;
using System.Collections.Generic;
using Classes;
using Menus;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

namespace Confrontation
{
    public class Confrontation : MonoBehaviourPunCallbacks
    {
        public GameObject prefabHealth;
        public GameObject prefabShield;
        public GameObject prefabEnergy;
        public GameObject prefabAttack;
        public GameObject prefabDefense;
        public GameObject prefabStarship;
        public GameObject prefabType;

        public GameObject prefabHealthEnemy;
        public GameObject prefabShieldEnemy;
        public GameObject prefabEnergyEnemy;
        public GameObject prefabAttackEnemy;
        public GameObject prefabDefenseEnemy;
        public GameObject prefabStarshipEnemy;
        public GameObject prefabTypeEnemy;

        public GameObject objectMyShip;
        public GameObject objectEnemyShip;

        /*
        TODO: store object in a struct. like following:
        dict myObjects {string prefab.tag: object, ...} foreach prefab up;
        Ex: myObject["Health"], myObjects["ShipType_enemy"]
        
        dict enemyObjects {string object.tag: object, ...} foreach photonView.gameObject where !photonView.IsMine;
        
        note: since prefab are only used for getting their names, only strore a array of strings containing names
        (store only the seven firsts, then add "_enemy" to them to get the seven others names;
        
        maybe store a array of (string name, Vector2 pos) instead
        also try prefab.transform and stuff like that
        don't forget to set the parent (only for myObjects of course)
        
        remove GetMineOnEnemyFromTag();
        */

        private Dictionary<string, GameObject> myObjects = new Dictionary<string, GameObject>();
        private Dictionary<string, GameObject> enemyObjects = new Dictionary<string, GameObject>();

        private Starship starship;
        private Starship starshipEnemy;

        private int ready;

        // Start is called before the first frame update
        void Start()
        {
            Debug.Log("Start Confrontation!");

            foreach (GameObject prefab in new[]
                     {
                         prefabHealth, prefabShield, prefabEnergy, prefabAttack, prefabDefense, prefabStarship,
                         prefabType, prefabHealthEnemy, prefabShieldEnemy, prefabEnergyEnemy, prefabAttackEnemy,
                         prefabDefenseEnemy, prefabStarshipEnemy, prefabTypeEnemy
                     })
            {
                GameObject o =
                    PhotonNetwork.Instantiate(prefab.name, prefab.transform.position, prefab.transform.rotation);
                o.transform.SetParent(objectMyShip.transform, false);
                myObjects.Add(prefab.tag, o);
            }
            


            Profile profile = LoadProfile.Load();
            starship = profile.MainStarship;
            int n = Convert.ToInt32(PhotonNetwork.LocalPlayer.NickName.Split(' ')[1]) % 100;
            starship.Refresh(n, n / 10, n);
            ready = 0;
        }

        // Update is called once per frame
        void Update()
        {
            int n = PhotonNetwork.ViewCount;
            if (n != 28)
            {
                Debug.Log($"Update Confrontation | photonViews: {n}, ready: {ready}");
            }

            switch (ready)
            {
                case 0:
                    if (SetMine())
                        ready++;
                    return;
                case 1 when n == 28:
                    if (SetMineOnEnemy())
                        ready++;
                    break;
                case 2:
                    if (GetEnemyStarship())
                        ready++;
                    return;
            }

            Refresh();
            starship.GainEnergy(8 * Time.deltaTime);
        }

        private PhotonView GetMineOnEnemyFromTag(string s)
        {
            foreach (PhotonView view in FindObjectsOfType<PhotonView>())
            {
                if (view.IsMine)
                    continue;
                if (view.gameObject.CompareTag(s))
                    return view;
            }

            Debug.LogError($"GetMineOnEnemyFromTag: Not found | tag: {s}");
            return null;
        }

        private bool SetMine()
        {
            Slider hpSlider = myObjects["Health"].GetComponent<Slider>();
            hpSlider.maxValue = starship.MaxHp;
            hpSlider.value = starship.Hp;
            Debug.Log($"Set hp to {hpSlider.value}/{hpSlider.maxValue}");

            Debug.Log($"Set shield to {starship.Absorption}/{starship.MaxAbsorption}");
            Slider shieldSlider = myObjects["Shield"].GetComponent<Slider>();
            shieldSlider.maxValue = starship.MaxAbsorption;
            shieldSlider.value = starship.Absorption;

            TMP_Text attackText = myObjects["Attack"].GetComponent<TMP_Text>();
            attackText.text = "Attack: " + starship.StatAttack;

            TMP_Text defenseText = myObjects["Defense"].GetComponent<TMP_Text>();
            defenseText.text = "Defense: " + starship.StatDefense;

            TMP_Text typeText = myObjects["ShipType"].GetComponent<TMP_Text>();
            typeText.text = "Ship Type: " + starship.Type;

            return true;
        }

        private bool SetMineOnEnemy()
        {
            GetMineOnEnemyFromTag("Health_enemy").RPC("SetHp", RpcTarget.Others, starship.Hp);
            return true;
        }

        private bool GetEnemyStarship()
        {
            int? hp = null;
            int? maxHp = null;
            int? attack = null;
            int? defense = null;
            float? energy = null;
            int? absorption = null;
            int? maxAbsorption = null;
            Starship.StarshipType? starshipType = null;

            foreach (PhotonView view in FindObjectsOfType<PhotonView>())
            {
                if (view.IsMine)
                    continue;
                GameObject o = view.gameObject;
                switch (o.tag)
                {
                    case "Health" when o:
                        hp = (int) o.GetComponent<Slider>().value;
                        maxHp = (int) o.GetComponent<Slider>().maxValue;
                        break;
                    case "Shield":
                        absorption = (int) o.GetComponent<Slider>().value;
                        maxAbsorption = (int) o.GetComponent<Slider>().maxValue;
                        break;
                    case "Energy":
                        energy = o.GetComponent<Image>().fillAmount;
                        break;
                    case "Attack":
                        int.TryParse(o.GetComponent<TMP_Text>().text.Split(' ')[1], out int att);
                        attack = att;
                        break;
                    case "Defense":
                        int.TryParse(o.GetComponent<TMP_Text>().text.Split(' ')[1], out int def);
                        defense = def;
                        break;
                    case "ShipSprite":
                        string s = o.GetComponent<Image>().sprite.name;
                        if (s.Contains("_0"))
                        {
                            starshipType = Starship.StarshipType.Fighter;
                        }
                        else if (s.Contains("_1"))
                        {
                            starshipType = Starship.StarshipType.Hunter;
                        }
                        else if (s.Contains("_2"))
                        {
                            starshipType = Starship.StarshipType.Cargo;
                        }

                        break;
                }
            }

            ValueType[] values = {hp, maxHp, attack, defense, energy, absorption, maxAbsorption, starshipType};
            string[] names =
                {"hp", "maxHp", "attack", "defense", "energy", "absorption", "maxAbsorption", "starshipType"};

            for (int i = 0; i < values.Length; i++)
            {
                if (values[i] != null) continue;
                Debug.LogError(names[i] + $" not defined yet! ({i}/{values.Length} succeeded)");
                return false;
            }

            starshipEnemy = new Starship((Starship.StarshipType) starshipType, (int) maxHp, (int) attack, (int) defense,
                (int) maxAbsorption);
            starshipEnemy.Refresh((int) hp, (int) absorption, (float) energy);
            starship.MatchEnemy(starshipEnemy);
            return true;
        }

        private void Refresh()
        {
            Slider hpSlider = myObjects["Health"].GetComponent<Slider>();
            hpSlider.value = starship.Hp;
            Debug.Log($"Refresh hp to {hpSlider.value}/{hpSlider.maxValue}\ninstead of {starship.Hp}/{starship.MaxHp}");

            Slider shieldSlider = myObjects["Shield"].GetComponent<Slider>();
            shieldSlider.value = starship.Absorption;

            myObjects["Energy"].GetComponent<Image>().fillAmount = (float) starship.Energy / 100;
            myObjects["Energy"].GetComponentInChildren<TMP_Text>().text = ((int) starship.Energy).ToString();
        }

        public void TaskOnClick(int action)
        {
            Starship.AttackType attackType = (Starship.AttackType) action;
            if (!Action(attackType))
            {
                // enemy starship is destroyed
                starship.EndConfrontation();
                starshipEnemy.EndConfrontation();

                // TODO: stop confrontation and start infiltration
                // Since infiltration isn't implemented yet, it will redirect to the main menu.
            }
        }

        private bool Action(Starship.AttackType attackType)
        {
            return attackType switch
            {
                Starship.AttackType.Force => starship.Energy < Starship.AttackForceCost || starship.AttackForce(),
                Starship.AttackType.Speed => starship.Energy < Starship.AttackSpeedCost || starship.AttackSpeed(),
                Starship.AttackType.Utility => starship.Energy < Starship.UtilityCost || starship.Utility(),
                _ => true
            };
        }
    }
}